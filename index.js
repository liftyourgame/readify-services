// index.js

const serverless = require('serverless-http')
require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
var cors = require('cors')
const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const RootController = require('./services/RootController')
const VersionController = require('./services/VersionController')
const TokenController = require('./services/TokenController')
const FibonacciController = require('./services/FibonacciController')
const ReverseWordsController = require('./services/ReverseWordsController')
const TriangleTypeController = require('./services/TriangleTypeController')

app.use('/', RootController)
app.use('/version', VersionController)
app.use('/api/Token', TokenController)
app.use('/api/Fibonacci', FibonacciController)
app.use('/api/ReverseWords', ReverseWordsController)
app.use('/api/TriangleType', TriangleTypeController)

module.exports.handler = serverless(app)
