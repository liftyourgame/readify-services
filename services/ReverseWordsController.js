// ReverseWordsController.js

const express = require('express')
const bodyParser = require('body-parser')
const router = new express.Router()
const cors = require('cors')


router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())
router.use(cors())


router.get('/', function (req, res) {
  var result = ""
  var sentence = req.query.sentence
  if (sentence) {
    var wordArray = sentence.split(" ");

    for (var i = 0; i < wordArray.length; i++) {
      wordArray[i] = wordArray[i].split("").reverse()
        .join("")
    }
  
    result = wordArray.join(" ")
  
    res.json(result)
  } else {
    res.json("")
  }
})

module.exports = router
