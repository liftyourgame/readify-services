// TokenController.js

const express = require('express')
const bodyParser = require('body-parser')
const router = new express.Router()
const cors = require('cors')


router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())
router.use(cors())


router.get('/', function (req, res) {
  res.json("47b168bf-1006-4925-a83e-95c1f052eb8f")
})

module.exports = router
