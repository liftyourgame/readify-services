// TriangleTypeController.js

const express = require('express')
const bodyParser = require('body-parser')
const router = new express.Router()
const cors = require('cors')
const validate = require('../utils/Validate')


router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())
router.use(cors())


router.get('/', function (req, res) {
  var result = ""
  var a = req.query.a
  var b = req.query.b
  var c = req.query.c
  

  if (!validate.isNormalInteger(a) || !validate.isNormalInteger(b) || !validate.isNormalInteger(c)) {
    res.status(400)
    res.json({"message":"The request is invalid."})
  } else if (!validate.validTriangle(a, b, c)) {
    result = "Error"
  } else if (a === b && b === c) {
    result = "Equilateral"
  } else if (a === b || a === c || b === c) {
    result = "Isosceles"
  } else {
    result = "Scalene"
  }

  res.json(result)
})

module.exports = router
