// RootController.js

const express = require('express')
const bodyParser = require('body-parser')
const router = new express.Router()
const cors = require('cors')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())
router.use(cors())


router.get('/', function (req, res) {
  res.send('Readify API')
})

module.exports = router
