// FibonacciController.js

const express = require('express')
const bodyParser = require('body-parser')
const router = new express.Router()
const cors = require('cors')
const validate = require('../utils/Validate')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())
router.use(cors())

router.get('/', function (req, res) {
  var result = 0
  var prev = 0
  var prevPrev = 0
  var n = req.query.n

  if (!n || n==="") {
    res.status(404)
    res.json({"message": "No HTTP resource was found that matches the request URI 'https://knockknock.readify.net/api/Fibonacci'."})
  } else if (isNaN(n) || !validate.isNormalInteger(n)) {
    res.status(400)
    res.json({"message": "The request is invalid."})
  } else {
    if (n<0) {
      n = -n
    }

    for (var i = 1; i <= n; i++) {
      if (i == 1) {
        result = 1
      } else {
        result = prevPrev + prev
      }
      prevPrev = prev
      prev = result
    }

    res.json(result)
  }
  
})

module.exports = router
