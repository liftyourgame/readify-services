var validate = {}

validate.isNormalInteger = str => {
  var n = Math.floor(Number(str))
  return n !== Infinity && String(n) === str
}

validate.isCardinalNumber = n => {
  if (n==="" || isNaN(n)) {
    return false
  }

  return n > 0
}

validate.validTriangle = (aStr, bStr, cStr) => {
  if (!validate.isCardinalNumber(aStr) || !validate.isCardinalNumber(bStr) || !validate.isCardinalNumber(cStr)) {
    return false
  }
  
  a = parseInt(aStr)
  b = parseInt(bStr)
  c = parseInt(cStr)

  if ((a+b)<=c || (a+c)<=b || (b+c)<=a) {
    return false
  }

  return true
}

module.exports = validate
